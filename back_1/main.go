package main

import (
	"log"
	"time"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

func HandlerRoute0(ctx *fasthttp.RequestCtx) {
	time.Sleep(500 * time.Millisecond)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.WriteString("Привет! Путь /. Сервер бэка 1")
}

func HandlerOne(ctx *fasthttp.RequestCtx) {
	time.Sleep(500 * time.Millisecond)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.WriteString("Привет! Путь /one. Сервер бэка 1")
}

func main() {
	log.Println("Server started")
	var router = fasthttprouter.New()
	router.GET("/", HandlerRoute0)
	router.GET("/one", HandlerOne)
	if err := fasthttp.ListenAndServe(":8000", router.Handler); err != nil {
		log.Fatalln(err.Error())
	}
}
