from http.server import HTTPServer, BaseHTTPRequestHandler
import time

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        time.sleep(2)
        self.wfile.write(b'Hello, from server2\n')


httpd = HTTPServer(("", 8000), SimpleHTTPRequestHandler)
httpd.serve_forever()
