package main

import (
	"log"
	"time"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

func HandlerRoute0(ctx *fasthttp.RequestCtx) {
	time.Sleep(500 * time.Millisecond)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.WriteString("Привет! Путь /. Сервер бэка 3")
}

func HandlerThree(ctx *fasthttp.RequestCtx) {
	time.Sleep(500 * time.Millisecond)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.WriteString("Привет! Путь /three. Сервер бэка 3")
}

func main() {
	log.Println("Server started")
	var router = fasthttprouter.New()
	router.GET("/", HandlerRoute0)
	router.GET("/three", HandlerThree)
	if err := fasthttp.ListenAndServe(":8000", router.Handler); err != nil {
		log.Fatalln(err.Error())
	}
}
